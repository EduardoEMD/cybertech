(function($) {
    "use strict";
    var secret = $("input[name='_token']").val();
    var host = $('#base').val();

    AOS.init({
        easing: 'ease-in-out-back',
        once: true
    });

    $('.file-uploader .attacher').click(function() {
        $(this).parent().find('input[type="file"]').trigger("click");
    })

    $('#cv-form').submit(function() {
        event.preventDefault();
        var fullname = $('[name="fullname"]').val();
        var phone = $('[name="phone"]').val();
        var email = $('[name="email"]').val();
        var message = $('[name="message"]').val();
        var job = $(".job-vacancy-title").text();

        if (!fullname.trim().length || !phone.trim().length || !email.trim().length || !message.trim().length) {

            Lobibox.notify('default', {
                title: 'ծանուցում',
                icon: false,
                pauseDelayOnHover: true,
                continueDelayOnInactiveTab: false,
                sound: false,
                msg: 'Խնդրում ենք պարտադիր լրացրեք բոլոր դաշտերը նախքան կուղարկեք նամակը'
            });
        } else {

            $('html,body,#cv-form input[type="submit"]').css({ cursor: "wait" });
            $('#cv-form input[type="submit"]').attr("disabled", true);

            var data = new FormData(document.getElementById('cv-form'));
            data.append("_token", secret);
            data.append("jobTitle", job);

            var config = {
                url: host + "/new-applicant",
                method: "POST",
                processData: false,
                contentType: false,
                data: data,
                success: r => {

                    $('html,body,#cv-form input[type="submit"]').css({ cursor: "default" });
                    $('#cv-form input[type="submit"]').removeAttr("disabled");

                    if (r == "sent") {
                        Lobibox.notify('success', {
                            title: 'ծանուցում',
                            icon: false,
                            pauseDelayOnHover: true,
                            continueDelayOnInactiveTab: false,
                            sound: false,
                            msg: 'Նամակը ուղարկված է'
                        });
                    } else {
                        Lobibox.notify('error', {
                            title: 'ծանուցում',
                            icon: false,
                            pauseDelayOnHover: true,
                            continueDelayOnInactiveTab: false,
                            sound: false,
                            msg: 'Առաջացավ խնդիր, խնդրում ենք փորձել մի փոքր ուշ։'
                        });
                    }
                }
            };

            $.ajax(config);
        }


    })

    $('#message-form').submit(function() {
            event.preventDefault();
            var fullname = $('#fullname').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var message = $('#message').val();

            if (!fullname.trim().length || !phone.trim().length || !email.trim().length || !message.trim().length) {
                Lobibox.notify('default', {
                    title: 'ծանուցում',
                    icon: false,
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    sound: false,
                    msg: 'Խնդրում ենք պարտադիր լրացրեք բոլոր դաշտերը նախքան կուղարկեք նամակը'
                });
            } else {
                $('html,body,.message-form .submit').css({ cursor: "wait" });
                $('.message-form .submit').attr("disabled", true);

                var data = {
                    fullname: fullname,
                    phone: phone,
                    email: email,
                    message: message,
                    _token: secret
                };
                var config = {
                    url: host + "/new-message",
                    method: "POST",
                    data: data,
                    success: r => {

                        $('html,body,.message-form .submit').css({ cursor: "default" });
                        $('.message-form .submit').removeAttr("disabled");

                        if (r == "sent") {
                            Lobibox.notify('success', {
                                title: 'ծանուցում',
                                icon: false,
                                pauseDelayOnHover: true,
                                continueDelayOnInactiveTab: false,
                                sound: false,
                                msg: 'Նամակը ուղարկված է'
                            });
                        } else {
                            Lobibox.notify('error', {
                                title: 'ծանուցում',
                                icon: false,
                                pauseDelayOnHover: true,
                                continueDelayOnInactiveTab: false,
                                sound: false,
                                msg: 'Առաջացավ խնդիր, խնդրում ենք փորձել մի փոքր ուշ։'
                            });
                        }
                    }
                };

                $.ajax(config);
            }

        })
        /*--
        Mobile Menu
        ------------------------*/
    $('.mobile-menu nav').meanmenu({
        meanScreenWidth: "990",
        meanMenuContainer: ".mobile-menu",
        onePage: true,
    });

    // $('.vacancy-items').slick({
    //   arrows:false
    // });
    // var right_arrow_template = `
    //   <div class="vacancy-item-button">
    //     <i class="zmdi zmdi-arrow-right"></i>
    //   </div>
    // `;
    // var left_arrow_template = `
    //   <div class="vacancy-item-button">
    //     <i class="zmdi zmdi-arrow-left"></i>
    //   </div>
    // `;

    $('.vacancy-items').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows: false,
    })


    $('.owl-carousel').owlCarousel({
        items: 3,
        autoplay: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            750: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    })

    /*--
    Magnific Popup
    ------------------------*/
    $('.video-popup').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        zoom: {
            enabled: true,
        }
    });


    /*--
    slick slider
    ------------------------*/
    $('.clients-slider-single').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.clients-slider'
    });

    $('.clients-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 4,
        centerMode: true,
        slidesToScroll: 1,
        asNavFor: '.clients-slider-single',
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });



    /*--
    slick slider
    ------------------------*/
    $('.slider-2').slick({
        centerMode: true,
        dots: true,
        centerPadding: '0',
        slidesToShow: 1,
        arrows: false,
        responsive: [{
                breakpoint: 970,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    /*--------------------------
    counterUp
    ---------------------------- */
    $('.timer').counterUp({
        delay: 10,
        time: 5000
    });

    /*----------------------------
    owl active brand-logo
    ------------------------------ */
    $('.item_all').owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: false,
        navigation: false,
        items: 6,
        /* transitionStyle : "fade", */
        /* [This code for animation ] */
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [980, 4],
        itemsTablet: [768, 4],
        itemsTablet: [767, 2],
        itemsMobile: [479, 2],
    });

    /*--
    Menu Stick
    -----------------------------------*/

    if ($('.sticker')[0]) {
        $('.sticker');
        $(window).scroll(function() {
            var wind_scr = $(window).scrollTop();
            var window_width = $(window).width();
            var head_w = $('.sticker').height();
            if (window_width >= 10) {
                if (wind_scr < 100) {
                    // $('.sticker').removeAttr('data-stick');
                    $('.sticker').attr('data-stick', true);

                } else {
                    $('.sticker').attr('data-stick', true);
                }
            }
        });

        $(window).scroll();

    };

    /*--
    One Page Nav
    -----------------------------------*/
    var top_offset = $('.main-menu').height() - -60;
    $('.main-menu nav ul').onePageNav({
        currentClass: 'active',
        scrollOffset: top_offset,
    });

    /*--
    Smooth Scroll
    -----------------------------------*/
    $('.scroll-down, .mean-nav ul li a').on('click', function(e) {
        e.preventDefault();
        var link = this;
        $.smoothScroll({
            offset: -100,
            scrollTarget: link.hash
        });
    });

    /*--------------------------
    scrollUp
    ---------------------------- */
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 200) {
            $("#toTop").fadeIn();
        } else {
            $("#toTop").fadeOut();
        }
    });
    $('#toTop').on('click', function() {
        $("html,body").animate({
            scrollTop: 0
        }, 500)
    });


    /*--
     MailChimp
    ------------------------*/
    $('#mc-form').ajaxChimp({
        language: 'en',
        callback: mailChimpResponse,
        // ADD YOUR MAILCHIMP URL BELOW HERE!
        url: 'http://themeshaven.us8.list-manage.com/subscribe/post?u=759ce8a8f4f1037e021ba2922&amp;id=a2452237f8'
    });

    function mailChimpResponse(resp) {
        if (resp.result === 'success') {
            $('.mailchimp-success').html('' + resp.msg).fadeIn(900);
            $('.mailchimp-error').fadeOut(400);
        } else if (resp.result === 'error') {
            $('.mailchimp-error').html('' + resp.msg).fadeIn(900);
        }
    }

    /*----------------------------
        home-slider
    ------------------------------ */
    $(".slider-active").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: false,
        navigation: true,
        items: 1,
        /* transitionStyle : "fade", */
        /* [This code for animation ] */
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [768, 1],
        itemsTablet: [767, 1],
        itemsMobile: [479, 1],
    });

    /*----------------------------
        text-animation
    ------------------------------ */
    $('.tlt1').textillate({
        loop: true,
        in: {
            effect: 'fadeInDown',
        },
        out: {
            effect: 'flip',
        },
    });

    /*----------------------------
        text-animation
    ------------------------------ */
    $('.tlt').textillate({
        loop: true,
        in: {
            effect: 'fadeInRight',
        },
        out: {
            effect: 'fadeOutLeft',
        },
    });

    /*----------------------------
    video
    ------------------------------ */
    $('.video-bg').vide({
        mp4: 'video/iphone7',
        webm: 'video/iphone7',
        poster: 'video/banner',
    }, {
        posterType: 'jpg',
        className: 'video-container'
    });

    /*----------------------------
    kenburne
    ------------------------------ */
    $('.slide-kenburne').kenburnsy({
        fullscreen: true
    });

    /*----------------------------
    youtube video
    ------------------------------ */
    $('.youtube-bg').YTPlayer({
        videoURL: "r4dD-WYzrMs",
        containment: '.youtube-bg',
        autoPlay: true,
        loop: true,
    });

    /*----------------------------
    owl active brand-logo
    ------------------------------ */
    $('.slider-img').owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: true,
        navigation: false,
        items: 1,
        /* transitionStyle : "fade", */
        /* [This code for animation ] */
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [768, 1],
        itemsTablet: [767, 1],
        itemsMobile: [479, 1],
    });

    /*----------------------------
    wow js active
    ------------------------------ */
    new WOW().init();



    //venbox start
    $('.venobox').venobox({
            numeratio: true,
            titleattr: 'data-title'
        })
        //venbox end

    $('.vacancy-open-modal').click(function() {
        var details = $(this).parents('.vacancy').find('.vacancy-details').text();
        var logo = $(this).parents('.vacancy').find('.vacancy-logo');
        var jobTitle = $(this).data("job");

        $('.vacancy-modal-logo').attr('src', logo.attr('src'));
        $('.vacancy-modal-details').text(details);
        $('.job-vacancy-title').text(jobTitle);
    })











})(jQuery);