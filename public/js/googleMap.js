var map;
function initMap() {
	let myLocation = {lat: 40.17033136628273, lng: 44.50798407669595};

	map = new google.maps.Map(document.getElementById('map'), {
		center: myLocation,
		zoom: 13
	});

	var icon = {
	    url: "img/map-marker.png", 
	    scaledSize: new google.maps.Size(30, 45)
	};


	var marker = new google.maps.Marker({
		position: myLocation,
		icon: icon,
		map: map
	});

	marker.setAnimation(google.maps.Animation.BOUNCE);
}