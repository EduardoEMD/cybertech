/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100133
 Source Host           : localhost:3306
 Source Schema         : cybertech

 Target Server Type    : MySQL
 Target Server Version : 100133
 File Encoding         : 65001

 Date: 04/07/2018 18:19:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `categories_slug_unique`(`slug`) USING BTREE,
  INDEX `categories_parent_id_foreign`(`parent_id`) USING BTREE,
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, NULL, 1, 'CyberTech News', 'CyberTech-News', '2018-07-01 07:20:42', '2018-07-01 07:20:42');
INSERT INTO `categories` VALUES (2, NULL, 1, 'Science', 'science', '2018-07-01 07:20:42', '2018-07-01 07:20:42');
INSERT INTO `categories` VALUES (3, NULL, 1, 'Apps & Gadgets', 'app-and-gadgets', NULL, NULL);
INSERT INTO `categories` VALUES (4, NULL, 1, 'Culture', 'culture', NULL, NULL);
INSERT INTO `categories` VALUES (5, NULL, 1, 'Entertainment', 'entertainment', NULL, NULL);

-- ----------------------------
-- Table structure for data_rows
-- ----------------------------
DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `data_rows_data_type_id_foreign`(`data_type_id`) USING BTREE,
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of data_rows
-- ----------------------------
INSERT INTO `data_rows` VALUES (1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1);
INSERT INTO `data_rows` VALUES (2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2);
INSERT INTO `data_rows` VALUES (3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '', 3);
INSERT INTO `data_rows` VALUES (4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '', 4);
INSERT INTO `data_rows` VALUES (5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '', 5);
INSERT INTO `data_rows` VALUES (6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 6);
INSERT INTO `data_rows` VALUES (7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7);
INSERT INTO `data_rows` VALUES (8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '', 8);
INSERT INTO `data_rows` VALUES (9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10);
INSERT INTO `data_rows` VALUES (10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11);
INSERT INTO `data_rows` VALUES (11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12);
INSERT INTO `data_rows` VALUES (12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '', 12);
INSERT INTO `data_rows` VALUES (13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1);
INSERT INTO `data_rows` VALUES (14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2);
INSERT INTO `data_rows` VALUES (15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3);
INSERT INTO `data_rows` VALUES (16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4);
INSERT INTO `data_rows` VALUES (17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1);
INSERT INTO `data_rows` VALUES (18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2);
INSERT INTO `data_rows` VALUES (19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3);
INSERT INTO `data_rows` VALUES (20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4);
INSERT INTO `data_rows` VALUES (21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5);
INSERT INTO `data_rows` VALUES (22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, '', 9);
INSERT INTO `data_rows` VALUES (23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1);
INSERT INTO `data_rows` VALUES (24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2);
INSERT INTO `data_rows` VALUES (25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3);
INSERT INTO `data_rows` VALUES (26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 4);
INSERT INTO `data_rows` VALUES (27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5);
INSERT INTO `data_rows` VALUES (28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '', 6);
INSERT INTO `data_rows` VALUES (29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7);
INSERT INTO `data_rows` VALUES (30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1);
INSERT INTO `data_rows` VALUES (31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2);
INSERT INTO `data_rows` VALUES (32, 5, 'category_id', 'text', 'Category', 0, 0, 1, 1, 1, 0, NULL, 3);
INSERT INTO `data_rows` VALUES (33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4);
INSERT INTO `data_rows` VALUES (34, 5, 'excerpt', 'text_area', 'Excerpt', 0, 0, 1, 1, 1, 1, NULL, 5);
INSERT INTO `data_rows` VALUES (35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6);
INSERT INTO `data_rows` VALUES (36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7);
INSERT INTO `data_rows` VALUES (37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8);
INSERT INTO `data_rows` VALUES (38, 5, 'meta_description', 'text_area', 'Meta Description', 0, 0, 1, 1, 1, 1, NULL, 9);
INSERT INTO `data_rows` VALUES (39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 0, 0, 1, 1, 1, 1, NULL, 10);
INSERT INTO `data_rows` VALUES (40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11);
INSERT INTO `data_rows` VALUES (41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12);
INSERT INTO `data_rows` VALUES (43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14);
INSERT INTO `data_rows` VALUES (44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15);
INSERT INTO `data_rows` VALUES (45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1);
INSERT INTO `data_rows` VALUES (46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2);
INSERT INTO `data_rows` VALUES (47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3);
INSERT INTO `data_rows` VALUES (48, 6, 'excerpt', 'text_area', 'Excerpt', 0, 0, 1, 1, 1, 1, NULL, 6);
INSERT INTO `data_rows` VALUES (49, 6, 'body', 'rich_text_box', 'Body', 0, 0, 1, 1, 1, 1, NULL, 7);
INSERT INTO `data_rows` VALUES (50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 8);
INSERT INTO `data_rows` VALUES (51, 6, 'meta_description', 'text', 'Meta Description', 0, 0, 1, 1, 1, 1, NULL, 9);
INSERT INTO `data_rows` VALUES (52, 6, 'meta_keywords', 'text', 'Meta Keywords', 0, 0, 1, 1, 1, 1, NULL, 10);
INSERT INTO `data_rows` VALUES (53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 11);
INSERT INTO `data_rows` VALUES (54, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12);
INSERT INTO `data_rows` VALUES (55, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13);
INSERT INTO `data_rows` VALUES (56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 14);
INSERT INTO `data_rows` VALUES (57, 6, 'icon', 'text', 'Icon', 0, 1, 1, 1, 1, 1, NULL, 5);
INSERT INTO `data_rows` VALUES (58, 6, 'type', 'select_dropdown', 'Type', 0, 1, 1, 1, 1, 1, '{\"options\":{\"product\":\"product\",\"service\":\"service\"}}', 4);
INSERT INTO `data_rows` VALUES (59, 5, 'data_analysis', 'select_dropdown', 'Data Analysis', 0, 1, 1, 1, 1, 1, '{\"default\":\"Popular Posts\",\"options\":{\"homepage\":\"Home Page\",\"popular\":\"Popular Posts\",\"blogslider\":\"Blog Main Slider\"}}', 13);

-- ----------------------------
-- Table structure for data_types
-- ----------------------------
DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `model_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `policy_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `controller` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `data_types_name_unique`(`name`) USING BTREE,
  UNIQUE INDEX `data_types_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of data_types
-- ----------------------------
INSERT INTO `data_types` VALUES (1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-07-01 07:20:31', '2018-07-01 07:20:31');
INSERT INTO `data_types` VALUES (2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-07-01 07:20:31', '2018-07-01 07:20:31');
INSERT INTO `data_types` VALUES (3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-07-01 07:20:31', '2018-07-01 07:20:31');
INSERT INTO `data_types` VALUES (4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-07-01 07:20:41', '2018-07-01 07:20:41');
INSERT INTO `data_types` VALUES (5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-01 07:20:42', '2018-07-04 17:02:35');
INSERT INTO `data_types` VALUES (6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-01 07:20:44', '2018-07-01 12:33:28');

-- ----------------------------
-- Table structure for menu_items
-- ----------------------------
DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `color` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `route` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `parameters` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `menu_items_menu_id_foreign`(`menu_id`) USING BTREE,
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_items
-- ----------------------------
INSERT INTO `menu_items` VALUES (1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.dashboard', NULL);
INSERT INTO `menu_items` VALUES (2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.media.index', NULL);
INSERT INTO `menu_items` VALUES (3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.users.index', NULL);
INSERT INTO `menu_items` VALUES (4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.roles.index', NULL);
INSERT INTO `menu_items` VALUES (5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2018-07-01 07:20:34', '2018-07-01 07:20:34', NULL, NULL);
INSERT INTO `menu_items` VALUES (6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.menus.index', NULL);
INSERT INTO `menu_items` VALUES (7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.database.index', NULL);
INSERT INTO `menu_items` VALUES (8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.compass.index', NULL);
INSERT INTO `menu_items` VALUES (9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.bread.index', NULL);
INSERT INTO `menu_items` VALUES (10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2018-07-01 07:20:34', '2018-07-01 07:20:34', 'voyager.settings.index', NULL);
INSERT INTO `menu_items` VALUES (11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2018-07-01 07:20:42', '2018-07-01 07:20:42', 'voyager.categories.index', NULL);
INSERT INTO `menu_items` VALUES (12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2018-07-01 07:20:43', '2018-07-01 07:20:43', 'voyager.posts.index', NULL);
INSERT INTO `menu_items` VALUES (13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2018-07-01 07:20:44', '2018-07-01 07:20:44', 'voyager.pages.index', NULL);
INSERT INTO `menu_items` VALUES (14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2018-07-01 07:20:46', '2018-07-01 07:20:46', 'voyager.hooks', NULL);
INSERT INTO `menu_items` VALUES (15, 2, 'Գլխավոր', '/cybertech/public', '_self', NULL, '#000000', NULL, 1, '2018-07-01 09:51:30', '2018-07-01 17:01:26', NULL, '');
INSERT INTO `menu_items` VALUES (16, 2, 'Ծառայություններ', '#service-area', '_self', NULL, '#000000', NULL, 2, '2018-07-01 09:51:57', '2018-07-01 09:53:26', NULL, '');
INSERT INTO `menu_items` VALUES (17, 2, 'Products', '#features-area', '_self', NULL, '#000000', NULL, 3, '2018-07-01 09:52:12', '2018-07-01 09:53:29', NULL, '');
INSERT INTO `menu_items` VALUES (18, 2, 'Մեր մասին', '#about-area', '_self', NULL, '#000000', NULL, 4, '2018-07-01 09:52:28', '2018-07-01 09:53:29', NULL, '');
INSERT INTO `menu_items` VALUES (19, 2, 'Բլոգ', '/cybertech/public/blog', '_self', NULL, '#000000', NULL, 5, '2018-07-01 09:52:54', '2018-07-01 17:01:39', NULL, '');
INSERT INTO `menu_items` VALUES (20, 2, 'Աշխատանք', '#vacancy-area', '_self', NULL, '#000000', NULL, 6, '2018-07-01 09:53:08', '2018-07-01 09:53:29', NULL, '');
INSERT INTO `menu_items` VALUES (21, 2, 'Կապ', '#contact-area', '_self', NULL, '#000000', NULL, 7, '2018-07-01 09:53:21', '2018-07-01 09:53:29', NULL, '');
INSERT INTO `menu_items` VALUES (23, 3, 'Գլխավոր', '/cybertech/public', '_self', NULL, '#000000', NULL, 16, '2018-07-01 13:16:58', '2018-07-01 13:16:58', NULL, '');
INSERT INTO `menu_items` VALUES (24, 3, 'Բլոգ', '/blog', '_self', NULL, '#000000', NULL, 17, '2018-07-01 13:17:26', '2018-07-01 13:17:26', NULL, '');
INSERT INTO `menu_items` VALUES (25, 4, 'Science', '/cybertech/public/blog?search=Science', '_self', NULL, '#000000', NULL, 2, '2018-07-02 04:00:01', '2018-07-04 15:04:32', NULL, '');
INSERT INTO `menu_items` VALUES (26, 4, 'CyberTech News', '/cybertech/public/blog?search=CyberTech-News', '_self', NULL, '#000000', NULL, 1, '2018-07-02 04:00:45', '2018-07-04 15:04:04', NULL, '');
INSERT INTO `menu_items` VALUES (27, 4, 'Apps & Gadgets', '/cybertech/public/blog?search=app-and-gadgets', '_self', NULL, '#000000', NULL, 3, '2018-07-04 15:04:26', '2018-07-04 15:04:47', NULL, '');
INSERT INTO `menu_items` VALUES (28, 4, 'Culture', '/cybertech/public/blog?search=culture', '_self', NULL, '#000000', NULL, 4, '2018-07-04 15:05:03', '2018-07-04 15:05:29', NULL, '');
INSERT INTO `menu_items` VALUES (29, 4, 'Entertainment', '/cybertech/public/blog?search=entertainment', '_self', NULL, '#000000', NULL, 5, '2018-07-04 15:05:10', '2018-07-04 15:05:49', NULL, '');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `menus_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 'admin', '2018-07-01 07:20:33', '2018-07-01 07:20:33');
INSERT INTO `menus` VALUES (2, 'main', '2018-07-01 09:49:44', '2018-07-01 09:49:44');
INSERT INTO `menus` VALUES (3, 'page-menu', '2018-07-01 13:15:40', '2018-07-01 13:15:40');
INSERT INTO `menus` VALUES (4, 'blog-menu', '2018-07-02 03:58:32', '2018-07-02 03:58:41');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2016_01_01_000000_add_voyager_user_fields', 1);
INSERT INTO `migrations` VALUES (4, '2016_01_01_000000_create_data_types_table', 1);
INSERT INTO `migrations` VALUES (5, '2016_05_19_173453_create_menu_table', 1);
INSERT INTO `migrations` VALUES (6, '2016_10_21_190000_create_roles_table', 1);
INSERT INTO `migrations` VALUES (7, '2016_10_21_190000_create_settings_table', 1);
INSERT INTO `migrations` VALUES (8, '2016_11_30_135954_create_permission_table', 1);
INSERT INTO `migrations` VALUES (9, '2016_11_30_141208_create_permission_role_table', 1);
INSERT INTO `migrations` VALUES (10, '2016_12_26_201236_data_types__add__server_side', 1);
INSERT INTO `migrations` VALUES (11, '2017_01_13_000000_add_route_to_menu_items_table', 1);
INSERT INTO `migrations` VALUES (12, '2017_01_14_005015_create_translations_table', 1);
INSERT INTO `migrations` VALUES (13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1);
INSERT INTO `migrations` VALUES (14, '2017_03_06_000000_add_controller_to_data_types_table', 1);
INSERT INTO `migrations` VALUES (15, '2017_04_21_000000_add_order_to_data_rows_table', 1);
INSERT INTO `migrations` VALUES (16, '2017_07_05_210000_add_policyname_to_data_types_table', 1);
INSERT INTO `migrations` VALUES (17, '2017_08_05_000000_add_group_to_settings_table', 1);
INSERT INTO `migrations` VALUES (18, '2017_11_26_013050_add_user_role_relationship', 1);
INSERT INTO `migrations` VALUES (19, '2017_11_26_015000_create_user_roles_table', 1);
INSERT INTO `migrations` VALUES (20, '2018_03_11_000000_add_user_settings', 1);
INSERT INTO `migrations` VALUES (21, '2018_03_14_000000_add_details_to_data_types_table', 1);
INSERT INTO `migrations` VALUES (22, '2018_03_16_000000_make_settings_value_nullable', 1);
INSERT INTO `migrations` VALUES (23, '2016_01_01_000000_create_pages_table', 2);
INSERT INTO `migrations` VALUES (24, '2016_01_01_000000_create_posts_table', 2);
INSERT INTO `migrations` VALUES (25, '2016_02_15_204651_create_categories_table', 2);
INSERT INTO `migrations` VALUES (26, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` enum('ACTIVE','INACTIVE') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '<i class=\"zmdi zmdi-favorite\"></i>',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `pages_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-07-01 07:20:44', '2018-07-01 07:20:44', '<i class=\"zmdi zmdi-favorite\"></i>', NULL);
INSERT INTO `pages` VALUES (2, 1, 'Mobile development', 'Mobile app development is the act or process by which a mobile app is developed for mobile devices, such as personal digital assistants, enterprise digital assistants or mobile phones. These applications can be pre-installed on phones during manufacturing platforms, or delivered as web applications using server-side or client-side processing', '<h3><span style=\"color: #808080;\">Some Heading</span></h3>\r\n<p><span style=\"color: #808080;\">As part of the development process, mobile&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"User interface\" href=\"https://en.wikipedia.org/wiki/User_interface\">user interface</a>&nbsp;(UI) design is also essential in the creation of mobile apps. Mobile UI considers constraints, contexts, screen, input, and mobility as outlines for design. The user is often the focus of interaction with their device, and the interface entails components of both hardware and software. User input allows for the users to manipulate a system, and device\'s output allows the system to indicate the effects of the users\' manipulation. Mobile UI design constraints include limited attention and form factors, such as a mobile device\'s screen size for a user\'s hand(s). Mobile UI contexts signal cues from user activity, such as location and scheduling that can be shown from user interactions within a mobile app. Overall, mobile UI design\'s goal is mainly for an understandable, user-friendly interface. The UI of mobile apps should: consider users\' limited attention, minimize keystrokes, and be task-oriented with a minimum set of functions. This functionality is supported by&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Mobile enterprise application platform\" href=\"https://en.wikipedia.org/wiki/Mobile_enterprise_application_platform\">mobile enterprise application platforms</a>&nbsp;or&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Integrated development environment\" href=\"https://en.wikipedia.org/wiki/Integrated_development_environment\">integrated development environments</a>&nbsp;(IDEs).</span></p>\r\n<p><span style=\"color: #808080;\">Mobile UIs, or front-ends, rely on mobile back-ends to support access to enterprise systems. The mobile back-end facilitates data routing, security, authentication, authorization, working off-line, and service orchestration. This functionality is supported by a mix of&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Middleware\" href=\"https://en.wikipedia.org/wiki/Middleware\">middleware</a>&nbsp;components including mobile app server,&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Mobile backend as a service\" href=\"https://en.wikipedia.org/wiki/Mobile_backend_as_a_service\">mobile backend as a service</a>&nbsp;(MBaaS), and&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Service-oriented architecture\" href=\"https://en.wikipedia.org/wiki/Service-oriented_architecture\">service-oriented architecture</a>&nbsp;(SOA) infrastructure.</span></p>\r\n<p><span style=\"color: #808080;\"> <img style=\"display: block; margin-left: auto; margin-right: auto;\" title=\"our team\" src=\"http://localhost/cybertech/public//storage/pages/July2018/1-crUVBchmi2O_Ep4aIjlhZw.jpg\" alt=\"\" width=\"700\" height=\"467\" /> </span></p>\r\n<p><span style=\"color: #808080;\">As part of the development process, mobile&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"User interface\" href=\"https://en.wikipedia.org/wiki/User_interface\">user interface</a>&nbsp;(UI) design is also essential in the creation of mobile apps. Mobile UI considers constraints, contexts, screen, input, and mobility as outlines for design. The user is often the focus of interaction with their device, and the interface entails components of both hardware and software. User input allows for the users to manipulate a system, and device\'s output allows the system to indicate the effects of the users\' manipulation. Mobile UI design constraints include limited attention and form factors, such as a mobile device\'s screen size for a user\'s hand(s). Mobile UI contexts signal cues from user activity, such as location and scheduling that can be shown from user interactions within a mobile app. Overall, mobile UI design\'s goal is mainly for an understandable, user-friendly interface. The UI of mobile apps should: consider users\' limited attention, minimize keystrokes, and be task-oriented with a minimum set of functions. This functionality is supported by&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Mobile enterprise application platform\" href=\"https://en.wikipedia.org/wiki/Mobile_enterprise_application_platform\">mobile enterprise application platforms</a>&nbsp;or&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Integrated development environment\" href=\"https://en.wikipedia.org/wiki/Integrated_development_environment\">integrated development environments</a>&nbsp;(IDEs).As part of the development process, mobile&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"User interface\" href=\"https://en.wikipedia.org/wiki/User_interface\">user interface</a>&nbsp;(UI) design is also essential in the creation of mobile apps. Mobile UI considers constraints, contexts, screen, input, and mobility as outlines for design. The user is often the focus of interaction with their device, and the interface entails components of both hardware and software. User input allows for the users to manipulate a system, and device\'s output allows the system to indicate the effects of the users\' manipulation. Mobile UI design constraints include limited attention and form factors, such as a mobile device\'s screen size for a user\'s hand(s). Mobile UI contexts signal cues from user activity, such as location and scheduling that can be shown from user interactions within a mobile app. Overall, mobile UI design\'s goal is mainly for an understandable, user-friendly interface. The UI of mobile apps should: consider users\' limited attention, minimize keystrokes, and be task-oriented with a minimum set of functions. This functionality is supported by&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Mobile enterprise application platform\" href=\"https://en.wikipedia.org/wiki/Mobile_enterprise_application_platform\">mobile enterprise application platforms</a>&nbsp;or&nbsp; <a style=\"background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #808080; text-decoration: underline;\" title=\"Integrated development environment\" href=\"https://en.wikipedia.org/wiki/Integrated_development_environment\">integrated development environments</a>&nbsp;(IDEs).</span></p>\r\n<p><span style=\"color: #808080;\"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam, non eligendi odit itaque optio magni nobis excepturi. Explicabo deserunt, odit delectus blanditiis repellendus laborum molestias ratione eum totam natus voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam, non eligendi odit itaque optio magni nobis excepturi. Explicabo deserunt, odit delectus blanditiis repellendus laborum molestias ratione eum totam natus voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam, non eligendi odit itaque optio magni nobis excepturi. Explicabo deserunt, odit delectus blanditiis repellendus laborum molestias ratione eum totam natus voluptates. <img style=\"float: right;\" src=\"http://localhost/cybertech/public//storage/pages/July2018/mobile_application_development.png\" alt=\"\" width=\"457\" height=\"338\" /> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam, non eligendi odit itaque optio magni nobis excepturi. Explicabo deserunt, odit delectus blanditiis repellendus laborum molestias ratione eum totam natus voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam, non eligendi odit itaque optio magni nobis excepturi. Explicabo deserunt, odit delectus blanditiis repellendus laborum molestias ratione eum totam natus voluptates. </span></p>', 'pages\\July2018\\R2dgo7nDKjVOYQ6lTdol.png', 'mobile-development', 'mobidev', 'mobile, programming', 'ACTIVE', '2018-07-01 10:44:48', '2018-07-01 13:01:28', '<i class=\"zmdi zmdi-smartphone-setup\"></i>', 'service');
INSERT INTO `pages` VALUES (3, 1, 'Web development', 'Mobile app development is the act or process by which a mobile app is developed for mobile devices, such as personal digital assistants, enterprise digital assistants or mobile phones. These applications can be pre-installed on phones during manufacturing platforms, or delivered as web applications using server-side or client-side processing', '<p><span style=\"color: #808080;\"><strong>Web development&nbsp;</strong>is a broad term for the work involved in developing a&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Web site\" href=\"https://en.wikipedia.org/wiki/Web_site\">web site</a>&nbsp;for the&nbsp;<a style=\"background: none; color: #808080;\" title=\"Internet\" href=\"https://en.wikipedia.org/wiki/Internet\">Internet</a>&nbsp;(<a style=\"background: none; color: #808080;\" title=\"World Wide Web\" href=\"https://en.wikipedia.org/wiki/World_Wide_Web\">World Wide Web</a>) or an&nbsp;<a style=\"background: none; color: #808080;\" title=\"Intranet\" href=\"https://en.wikipedia.org/wiki/Intranet\">intranet</a>&nbsp;(a private network). Web development can range from developing the simplest static single page of&nbsp;<a style=\"background: none; color: #808080;\" title=\"Plain text\" href=\"https://en.wikipedia.org/wiki/Plain_text\">plain text</a>&nbsp;to the most complex web-based&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Internet application\" href=\"https://en.wikipedia.org/wiki/Internet_application\">internet applications</a>&nbsp;(or just \'web apps\')&nbsp;<a style=\"background: none; color: #808080;\" title=\"Electronic business\" href=\"https://en.wikipedia.org/wiki/Electronic_business\">electronic businesses</a>, and&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Social network service\" href=\"https://en.wikipedia.org/wiki/Social_network_service\">social network services</a>. A more comprehensive list of tasks to which web development commonly refers, may include&nbsp;<a style=\"background: none; color: #808080;\" title=\"Web engineering\" href=\"https://en.wikipedia.org/wiki/Web_engineering\">web engineering</a>,&nbsp;<a style=\"background: none; color: #808080;\" title=\"Web design\" href=\"https://en.wikipedia.org/wiki/Web_design\">web design</a>,&nbsp;<a style=\"background: none; color: #808080;\" title=\"Web content development\" href=\"https://en.wikipedia.org/wiki/Web_content_development\">web content development</a>, client liaison,&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Client-side scripting\" href=\"https://en.wikipedia.org/wiki/Client-side_scripting\">client-side</a>/<a style=\"background: none; color: #808080;\" title=\"Server-side scripting\" href=\"https://en.wikipedia.org/wiki/Server-side_scripting\">server-side</a>&nbsp;<a style=\"background: none; color: #808080;\" title=\"Computer programming\" href=\"https://en.wikipedia.org/wiki/Computer_programming\">scripting</a>,&nbsp;<a style=\"background: none; color: #808080;\" title=\"Web server\" href=\"https://en.wikipedia.org/wiki/Web_server\">web server</a>&nbsp;and&nbsp;<a style=\"background: none; color: #808080;\" title=\"Network security\" href=\"https://en.wikipedia.org/wiki/Network_security\">network security</a>&nbsp;configuration, and&nbsp;<a style=\"background: none; color: #808080;\" title=\"E-commerce\" href=\"https://en.wikipedia.org/wiki/E-commerce\">e-commerce</a>&nbsp;development. Among web professionals, \"web development\" usually refers to the main non-design aspects of building web sites: writing&nbsp;<a style=\"background: none; color: #808080;\" title=\"Markup language\" href=\"https://en.wikipedia.org/wiki/Markup_language\">markup</a>&nbsp;and&nbsp;<a style=\"background: none; color: #808080;\" title=\"Computer programming\" href=\"https://en.wikipedia.org/wiki/Computer_programming\">coding</a>. Most recently Web development has come to mean the creation of&nbsp;<a style=\"background: none; color: #808080;\" title=\"Content management system\" href=\"https://en.wikipedia.org/wiki/Content_management_system\">content management systems</a>&nbsp;or CMS. These CMS can be made from scratch, proprietary or open source. In broad terms the CMS acts as middleware between the database and the user through the browser. A principle benefit of a CMS is that it allows non-technical people to make changes to their web site without having technical knowledge.</span></p>\r\n<p><span style=\"color: #808080;\">For larger organizations and businesses, web development teams can consist of hundreds of people (<a style=\"background: none; color: #808080;\" title=\"Web developer\" href=\"https://en.wikipedia.org/wiki/Web_developer\">web developers</a>) and follow standard methods like&nbsp;<a style=\"background: none; color: #808080;\" title=\"Agile software development\" href=\"https://en.wikipedia.org/wiki/Agile_software_development\">Agile methodologies</a>&nbsp;while developing websites. Smaller organizations may only require a single permanent or contracting developer, or secondary assignment to related job positions such as a&nbsp;<a style=\"background: none; color: #808080;\" title=\"Graphic designer\" href=\"https://en.wikipedia.org/wiki/Graphic_designer\">graphic designer</a>&nbsp;or&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Information systems\" href=\"https://en.wikipedia.org/wiki/Information_systems\">information systems</a>&nbsp;technician. Web development may be a collaborative effort between departments rather than the domain of a designated department. There are three kinds of web developer specialization:&nbsp;<a style=\"background: none; color: #808080;\" title=\"Front-end web development\" href=\"https://en.wikipedia.org/wiki/Front-end_web_development\">front-end developer</a>, back-end developer, and&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Full-stack developer\" href=\"https://en.wikipedia.org/wiki/Full-stack_developer\">full-stack developer</a>. Front-end developers deal with the layout and visuals of a website, while back-end developers deal with the functionality of a website. Back-end developers will program in the functions of a website that will collect data.</span></p>\r\n<p><span style=\"color: #808080;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://localhost/cybertech/public//storage/pages/July2018/Web-Design-and-Development-large.jpg\" alt=\"\" width=\"900\" height=\"300\" /></span></p>\r\n<p><span style=\"color: #808080;\">For larger organizations and businesses, web development teams can consist of hundreds of people (<a style=\"background: none; color: #808080;\" title=\"Web developer\" href=\"https://en.wikipedia.org/wiki/Web_developer\">web developers</a>) and follow standard methods like&nbsp;<a style=\"background: none; color: #808080;\" title=\"Agile software development\" href=\"https://en.wikipedia.org/wiki/Agile_software_development\">Agile methodologies</a>&nbsp;while developing websites. Smaller organizations may only require a single permanent or contracting developer, or secondary assignment to related job positions such as a&nbsp;<a style=\"background: none; color: #808080;\" title=\"Graphic designer\" href=\"https://en.wikipedia.org/wiki/Graphic_designer\">graphic designer</a>&nbsp;or&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Information systems\" href=\"https://en.wikipedia.org/wiki/Information_systems\">information systems</a>&nbsp;technician. Web development may be a collaborative effort between departments rather than the domain of a designated department. There are three kinds of web developer specialization:&nbsp;<a style=\"background: none; color: #808080;\" title=\"Front-end web development\" href=\"https://en.wikipedia.org/wiki/Front-end_web_development\">front-end developer</a>, back-end developer, and&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Full-stack developer\" href=\"https://en.wikipedia.org/wiki/Full-stack_developer\">full-stack developer</a>. Front-end developers deal with the layout and visuals of a website, while back-end developers deal with the functionality of a website. Back-end developers will program in the functions of a website that will collect data.For larger organizations and businesses, web development teams can consist of hundreds of people (<a style=\"background: none; color: #808080;\" title=\"Web developer\" href=\"https://en.wikipedia.org/wiki/Web_developer\">web developers</a>) and follow standard methods like&nbsp;<a style=\"background: none; color: #808080;\" title=\"Agile software development\" href=\"https://en.wikipedia.org/wiki/Agile_software_development\">Agile methodologies</a>&nbsp;while developing websites. Smaller organizations may only require a single permanent or contracting developer, or secondary assignment to related job positions such as a&nbsp;<a style=\"background: none; color: #808080;\" title=\"Graphic designer\" href=\"https://en.wikipedia.org/wiki/Graphic_designer\">graphic designer</a>&nbsp;or&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Information systems\" href=\"https://en.wikipedia.org/wiki/Information_systems\">information systems</a>&nbsp;technician. Web development may be a collaborative effort between departments rather than the domain of a designated department. There are three kinds of web developer specialization:&nbsp;<a style=\"background: none; color: #808080;\" title=\"Front-end web development\" href=\"https://en.wikipedia.org/wiki/Front-end_web_development\">front-end developer</a>, back-end developer, and&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Full-stack developer\" href=\"https://en.wikipedia.org/wiki/Full-stack_developer\">full-stack developer</a>. Front-end developers deal with the layout and visuals of a website, while back-end developers deal with the functionality of a website. Back-end developers will program in the functions of a website that will collect data.For larger organizations and businesses, web development teams can consist of hundreds of people (<a style=\"background: none; color: #808080;\" title=\"Web developer\" href=\"https://en.wikipedia.org/wiki/Web_developer\">web developers</a>) and follow standard methods like&nbsp;<a style=\"background: none; color: #808080;\" title=\"Agile software development\" href=\"https://en.wikipedia.org/wiki/Agile_software_development\">Agile methodologies</a>&nbsp;while developing websites. Smaller organizations may only require a single permanent or contracting developer, or secondary assignment to related job positions such as a&nbsp;<a style=\"background: none; color: #808080;\" title=\"Graphic designer\" href=\"https://en.wikipedia.org/wiki/Graphic_designer\">graphic designer</a>&nbsp;or&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Information systems\" href=\"https://en.wikipedia.org/wiki/Information_systems\">information systems</a>&nbsp;technician. Web development may be a collaborative effort between departments rather than the domain of a designated department. There are three kinds of web developer specialization:&nbsp;<a style=\"background: none; color: #808080;\" title=\"Front-end web development\" href=\"https://en.wikipedia.org/wiki/Front-end_web_development\">front-end developer</a>, back-end developer, and&nbsp;<a class=\"mw-redirect\" style=\"background: none; color: #808080;\" title=\"Full-stack developer\" href=\"https://en.wikipedia.org/wiki/Full-stack_developer\">full-stack developer</a>. Front-end developers deal with the layout and visuals of a website, while back-end developers deal with the functionality of a website. Back-end developers will program in the functions of a website that will collect data.</span></p>\r\n<p><span style=\"color: #808080;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://localhost/cybertech/public//storage/pages/July2018/b2ap3_large_quality-web-design-fwd.jpg\" alt=\"\" width=\"850\" height=\"340\" /></span></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'pages\\July2018\\4YJ7WTY6FaxBCOgvBqRG.jpg', 'web-development', 'create your sites with us', 'key1, key2', 'ACTIVE', '2018-07-01 12:58:18', '2018-07-01 12:58:18', '<i class=\"zmdi zmdi-desktop-mac\"></i>', 'service');
INSERT INTO `pages` VALUES (4, 1, 'Social media marketing', 'Social media marketing is the use of social media platforms and websites to promote a product or service.[1] Although the terms e-marketing and digital marketing are still dominant in academia, social media marketing is becoming more popular for both practitioners and researchers.[2] Most social media platforms have built-in data analytics tools, which enable c', '<h1>Social Media Markeing</h1>\r\n<p><span style=\"color: #808080;\">Social media marketing&nbsp;is the use of&nbsp;<a style=\"text-decoration: underline; color: #808080; background: none;\" title=\"Social media\" href=\"https://en.wikipedia.org/wiki/Social_media\">social media</a>&nbsp;platforms and&nbsp;<a style=\"text-decoration: underline; color: #808080; background: none;\" title=\"Website\" href=\"https://en.wikipedia.org/wiki/Website\">websites</a>&nbsp;to promote a product or service.<sup id=\"cite_ref-1\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px;\"><a style=\"text-decoration: underline; color: #808080; background: none;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-1\">[1]</a></sup>&nbsp;Although the terms&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: underline; color: #808080; background: none;\" title=\"E-marketing\" href=\"https://en.wikipedia.org/wiki/E-marketing\">e-marketing</a>&nbsp;and&nbsp;<a style=\"text-decoration: underline; color: #808080; background: none;\" title=\"Digital marketing\" href=\"https://en.wikipedia.org/wiki/Digital_marketing\">digital marketing</a>&nbsp;are still dominant in academia, social media marketing is becoming more popular for both practitioners and researchers.<sup id=\"cite_ref-2\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px;\"><a style=\"text-decoration: underline; color: #808080; background: none;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-2\">[2]</a></sup>&nbsp;Most social media platforms have built-in data analytics tools, which enable companies to track the progress, success, and engagement of ad campaigns. Companies address a range of stakeholders through social media marketing, including current and potential customers, current and potential employees,&nbsp;<a style=\"text-decoration: underline; color: #808080; background: none;\" title=\"Journalist\" href=\"https://en.wikipedia.org/wiki/Journalist\">journalists</a>,&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: underline; color: #808080; background: none;\" title=\"Blogger\" href=\"https://en.wikipedia.org/wiki/Blogger\">bloggers</a>, and the general public. On a strategic level, social media marketing includes the management of a marketing campaign,&nbsp;<a style=\"text-decoration: underline; color: #808080; background: none;\" title=\"Governance\" href=\"https://en.wikipedia.org/wiki/Governance\">governance</a>, setting the scope (e.g. more active or passive use) and the establishment of a firm\'s desired social media \"culture\" and \"tone.\"</span></p>\r\n<p><span style=\"color: #808080;\">When using social media marketing, firms can allow customers and Internet users to post&nbsp;<a style=\"text-decoration: underline; color: #808080; background: none;\" title=\"User-generated content\" href=\"https://en.wikipedia.org/wiki/User-generated_content\">user-generated content</a>&nbsp;(e.g., online comments, product reviews, etc.), also known as \"<a style=\"text-decoration: underline; color: #808080; background: none;\" title=\"Earned media\" href=\"https://en.wikipedia.org/wiki/Earned_media\">earned media</a>,\" rather than use marketer-prepared&nbsp;<a style=\"text-decoration: underline; color: #808080; background: none;\" title=\"Advertising\" href=\"https://en.wikipedia.org/wiki/Advertising\">advertising</a>&nbsp;copy.</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #808080;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://localhost/cybertech/public/storage/pages/July2018/smo1.jpg\" alt=\"\" width=\"666\" height=\"333\" /><br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"background-color: #ffffff; color: #808080;\"><a class=\"mw-redirect\" style=\"color: #808080; background: none #ffffff;\" title=\"Social networking websites\" href=\"https://en.wikipedia.org/wiki/Social_networking_websites\">Social networking websites</a>&nbsp;allow individuals, businesses and other organizations to interact with one another and build relationships and communities online. When companies join these social channels, consumers can interact with them directly.<sup id=\"cite_ref-3\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; background-color: #ffffff;\"><a style=\"color: #808080; background: none #ffffff;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-3\">[3]</a></sup>&nbsp;That interaction can be more personal to users than traditional methods of&nbsp;<a style=\"color: #808080; background: none #ffffff;\" title=\"Marketing communications\" href=\"https://en.wikipedia.org/wiki/Marketing_communications\">outbound marketing</a>&nbsp;and advertising.<sup id=\"cite_ref-Assaad_4-0\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; background-color: #ffffff;\"><a style=\"color: #808080; background: none #ffffff;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-Assaad-4\">[4]</a></sup>&nbsp;Social networking sites act as&nbsp;<a style=\"color: #808080; background: none #ffffff;\" title=\"Word of mouth\" href=\"https://en.wikipedia.org/wiki/Word_of_mouth\">word of mouth</a>&nbsp;or more precisely, e-word of mouth. The Internet\'s ability to reach billions across the globe has given online word of mouth a powerful voice and far reach. The ability to rapidly change buying patterns and product or service acquisition and activity to a growing number of consumers is defined as an influence network.<sup id=\"cite_ref-Change_5-0\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; background-color: #ffffff;\"><a style=\"color: #808080; background: none #ffffff;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-Change-5\">[5]</a></sup>&nbsp;Social networking sites and&nbsp;<a class=\"mw-redirect\" style=\"color: #808080; background: none #ffffff;\" title=\"Blogs\" href=\"https://en.wikipedia.org/wiki/Blogs\">blogs</a>&nbsp;allow followers to \"retweet\" or \"repost\" comments made by others about a product being promoted, which occurs quite frequently on some social media sites.<sup id=\"cite_ref-faculty.ist.psu.edu_6-0\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; background-color: #ffffff;\"><a style=\"color: #808080; background: none #ffffff;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-faculty.ist.psu.edu-6\">[6]</a></sup>&nbsp;By repeating the message, the user\'s connections are able to see the message, therefore reaching more people. Because the information about the product is being put out there and is getting repeated, more traffic is brought to the product/company.<sup id=\"cite_ref-Assaad_4-1\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; background-color: #ffffff;\"><a style=\"color: #808080; background: none #ffffff;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-Assaad-4\">[4]</a></sup></span></p>\r\n<p><span style=\"background-color: #ffffff; color: #808080;\">Social networking websites are based on building virtual communities that allow consumers to express their needs, wants and values, online. Social media marketing then connects these consumers and audiences to businesses that share the same needs, wants, and values. Through social networking sites, companies can keep in touch with individual followers. This personal interaction can instill a feeling of&nbsp;<a style=\"color: #808080; background: none #ffffff;\" title=\"Loyalty\" href=\"https://en.wikipedia.org/wiki/Loyalty\">loyalty</a>&nbsp;into followers and potential customers. Also, by choosing whom to follow on these sites, products can reach a very narrow target audience.<sup id=\"cite_ref-Assaad_4-2\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; background-color: #ffffff;\"><a style=\"color: #808080; background: none #ffffff;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-Assaad-4\">[4]</a></sup>&nbsp;Social networking sites also include much information about what products and services prospective clients might be interested in. Through the use of new&nbsp;<a style=\"color: #808080; background: none #ffffff;\" title=\"Semantics\" href=\"https://en.wikipedia.org/wiki/Semantics\">semantic</a>&nbsp;analysis technologies, marketers can detect buying signals, such as content shared by people and questions posted online. An understanding of buying signals can help sales people target relevant prospects and marketers run micro-targeted campaigns.</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"background-color: #ffffff; color: #808080;\">In 2014, over 80% of business executives identified social media as an integral part of their business.<sup id=\"cite_ref-7\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; background-color: #ffffff;\"><a style=\"color: #808080; background: none #ffffff;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-7\">[7]</a></sup>&nbsp;Business retailers have seen 133% increases in their revenues from social media marketing.<sup id=\"cite_ref-8\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; background-color: #ffffff;\"><a style=\"color: #808080; background: none #ffffff;\" href=\"https://en.wikipedia.org/wiki/Social_media_marketing#cite_note-8\">[8]</a></sup></span></p>\r\n<p style=\"margin: 0.5em 0px; line-height: inherit; color: #222222; font-family: sans-serif;\">&nbsp;</p>\r\n<p>&nbsp;</p>', 'pages\\July2018\\D1poPs1OLIlOVCp05KKS.jpg', 'social-media-marketing', 'Start with us', 'asd1,asd12', 'ACTIVE', '2018-07-01 13:07:59', '2018-07-02 12:09:44', '<i class=\"zmdi zmdi-share\"></i>', 'service');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `permission_role_permission_id_index`(`permission_id`) USING BTREE,
  INDEX `permission_role_role_id_index`(`role_id`) USING BTREE,
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES (1, 1);
INSERT INTO `permission_role` VALUES (2, 1);
INSERT INTO `permission_role` VALUES (3, 1);
INSERT INTO `permission_role` VALUES (4, 1);
INSERT INTO `permission_role` VALUES (5, 1);
INSERT INTO `permission_role` VALUES (6, 1);
INSERT INTO `permission_role` VALUES (7, 1);
INSERT INTO `permission_role` VALUES (8, 1);
INSERT INTO `permission_role` VALUES (9, 1);
INSERT INTO `permission_role` VALUES (10, 1);
INSERT INTO `permission_role` VALUES (11, 1);
INSERT INTO `permission_role` VALUES (12, 1);
INSERT INTO `permission_role` VALUES (13, 1);
INSERT INTO `permission_role` VALUES (14, 1);
INSERT INTO `permission_role` VALUES (15, 1);
INSERT INTO `permission_role` VALUES (16, 1);
INSERT INTO `permission_role` VALUES (17, 1);
INSERT INTO `permission_role` VALUES (18, 1);
INSERT INTO `permission_role` VALUES (19, 1);
INSERT INTO `permission_role` VALUES (20, 1);
INSERT INTO `permission_role` VALUES (21, 1);
INSERT INTO `permission_role` VALUES (22, 1);
INSERT INTO `permission_role` VALUES (23, 1);
INSERT INTO `permission_role` VALUES (24, 1);
INSERT INTO `permission_role` VALUES (25, 1);
INSERT INTO `permission_role` VALUES (26, 1);
INSERT INTO `permission_role` VALUES (27, 1);
INSERT INTO `permission_role` VALUES (28, 1);
INSERT INTO `permission_role` VALUES (29, 1);
INSERT INTO `permission_role` VALUES (30, 1);
INSERT INTO `permission_role` VALUES (31, 1);
INSERT INTO `permission_role` VALUES (32, 1);
INSERT INTO `permission_role` VALUES (33, 1);
INSERT INTO `permission_role` VALUES (34, 1);
INSERT INTO `permission_role` VALUES (35, 1);
INSERT INTO `permission_role` VALUES (36, 1);
INSERT INTO `permission_role` VALUES (37, 1);
INSERT INTO `permission_role` VALUES (38, 1);
INSERT INTO `permission_role` VALUES (39, 1);
INSERT INTO `permission_role` VALUES (40, 1);
INSERT INTO `permission_role` VALUES (41, 1);

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `permissions_key_index`(`key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'browse_admin', NULL, '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (2, 'browse_bread', NULL, '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (3, 'browse_database', NULL, '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (4, 'browse_media', NULL, '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (5, 'browse_compass', NULL, '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (6, 'browse_menus', 'menus', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (7, 'read_menus', 'menus', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (8, 'edit_menus', 'menus', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (9, 'add_menus', 'menus', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (10, 'delete_menus', 'menus', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (11, 'browse_roles', 'roles', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (12, 'read_roles', 'roles', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (13, 'edit_roles', 'roles', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (14, 'add_roles', 'roles', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (15, 'delete_roles', 'roles', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (16, 'browse_users', 'users', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (17, 'read_users', 'users', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (18, 'edit_users', 'users', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (19, 'add_users', 'users', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (20, 'delete_users', 'users', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (21, 'browse_settings', 'settings', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (22, 'read_settings', 'settings', '2018-07-01 07:20:35', '2018-07-01 07:20:35');
INSERT INTO `permissions` VALUES (23, 'edit_settings', 'settings', '2018-07-01 07:20:36', '2018-07-01 07:20:36');
INSERT INTO `permissions` VALUES (24, 'add_settings', 'settings', '2018-07-01 07:20:36', '2018-07-01 07:20:36');
INSERT INTO `permissions` VALUES (25, 'delete_settings', 'settings', '2018-07-01 07:20:36', '2018-07-01 07:20:36');
INSERT INTO `permissions` VALUES (26, 'browse_categories', 'categories', '2018-07-01 07:20:42', '2018-07-01 07:20:42');
INSERT INTO `permissions` VALUES (27, 'read_categories', 'categories', '2018-07-01 07:20:42', '2018-07-01 07:20:42');
INSERT INTO `permissions` VALUES (28, 'edit_categories', 'categories', '2018-07-01 07:20:42', '2018-07-01 07:20:42');
INSERT INTO `permissions` VALUES (29, 'add_categories', 'categories', '2018-07-01 07:20:42', '2018-07-01 07:20:42');
INSERT INTO `permissions` VALUES (30, 'delete_categories', 'categories', '2018-07-01 07:20:42', '2018-07-01 07:20:42');
INSERT INTO `permissions` VALUES (31, 'browse_posts', 'posts', '2018-07-01 07:20:43', '2018-07-01 07:20:43');
INSERT INTO `permissions` VALUES (32, 'read_posts', 'posts', '2018-07-01 07:20:43', '2018-07-01 07:20:43');
INSERT INTO `permissions` VALUES (33, 'edit_posts', 'posts', '2018-07-01 07:20:43', '2018-07-01 07:20:43');
INSERT INTO `permissions` VALUES (34, 'add_posts', 'posts', '2018-07-01 07:20:43', '2018-07-01 07:20:43');
INSERT INTO `permissions` VALUES (35, 'delete_posts', 'posts', '2018-07-01 07:20:43', '2018-07-01 07:20:43');
INSERT INTO `permissions` VALUES (36, 'browse_pages', 'pages', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `permissions` VALUES (37, 'read_pages', 'pages', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `permissions` VALUES (38, 'edit_pages', 'pages', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `permissions` VALUES (39, 'add_pages', 'pages', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `permissions` VALUES (40, 'delete_pages', 'pages', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `permissions` VALUES (41, 'browse_hooks', NULL, '2018-07-01 07:20:46', '2018-07-01 07:20:46');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `data_analysis` enum('homepage','blogslider','popular','') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'popular',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `posts_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, 1, 3, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 'popular', 0, '2018-07-01 07:20:43', '2018-07-04 17:54:10');
INSERT INTO `posts` VALUES (2, 1, 2, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n<h2>We can use all kinds of format!</h2>\r\n<p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 'blogslider', 0, '2018-07-01 07:20:44', '2018-07-04 17:26:04');
INSERT INTO `posts` VALUES (3, 1, 2, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 'blogslider', 0, '2018-07-01 07:20:44', '2018-07-04 17:16:08');
INSERT INTO `posts` VALUES (4, 1, 1, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 'popular', 0, '2018-07-01 07:20:44', '2018-07-04 17:52:00');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `roles_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'admin', 'Administrator', '2018-07-01 07:20:34', '2018-07-01 07:20:34');
INSERT INTO `roles` VALUES (2, 'user', 'Normal User', '2018-07-01 07:20:35', '2018-07-01 07:20:35');

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `settings_key_unique`(`key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (1, 'site.title', 'Site Title', 'CyberTech', '', 'text', 1, 'Site');
INSERT INTO `settings` VALUES (2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site');
INSERT INTO `settings` VALUES (3, 'site.logo', 'Site Logo', 'settings\\July2018\\aH3xuAIB8Hk8qme85WnW.png', '', 'image', 3, 'Site');
INSERT INTO `settings` VALUES (4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site');
INSERT INTO `settings` VALUES (5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin');
INSERT INTO `settings` VALUES (6, 'admin.title', 'Admin Title', 'CyberTech Administration', '', 'text', 1, 'Admin');
INSERT INTO `settings` VALUES (7, 'admin.description', 'Admin Description', 'Welcome to Control Panel', '', 'text', 2, 'Admin');
INSERT INTO `settings` VALUES (8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin');
INSERT INTO `settings` VALUES (9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin');
INSERT INTO `settings` VALUES (10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- ----------------------------
-- Table structure for translations
-- ----------------------------
DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `translations_table_name_column_name_foreign_key_locale_unique`(`table_name`, `column_name`, `foreign_key`, `locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of translations
-- ----------------------------
INSERT INTO `translations` VALUES (1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `translations` VALUES (2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `translations` VALUES (3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `translations` VALUES (4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-07-01 07:20:44', '2018-07-01 07:20:44');
INSERT INTO `translations` VALUES (5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-07-01 07:20:45', '2018-07-01 07:20:45');
INSERT INTO `translations` VALUES (30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-07-01 07:20:45', '2018-07-01 07:20:45');

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles`  (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `user_roles_user_id_index`(`user_id`) USING BTREE,
  INDEX `user_roles_role_id_index`(`role_id`) USING BTREE,
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'users/default.png',
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `settings` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  INDEX `users_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 1, 'Eduard', 'eduard@admin.com', 'users\\July2018\\UWUcAwjtuWtyokwjgUf9.jpg', '$2y$10$DCGIojYdMn1DwJ0itBZrbOQ831ttsg7bX/UhFR6mLC7zIX1fCeD4y', 'OlIoxZ9h6alfwVHkrymLAVEOZDZrZtxSVpYrSxnlOEvx0haPC2SmjbhodjWT', '{\"locale\":\"en\"}', '2018-07-01 07:20:42', '2018-07-02 12:23:42');

SET FOREIGN_KEY_CHECKS = 1;
