@extends("layouts.index")

@section('menu')
    {{ menu('page-menu', 'menus.main-menu-template') }}
@stop

@section("content")
    <link rel="stylesheet" href="{{ asset('css/page.css') }}">
    <div id="home-area" class="bg-oapcity-40 bg-overly bg-img-1 sm-height-none">
        <img src="{{ asset('storage/'.$page->image) }}" >
        <div class="page-bannertext">
            <div class="slider-text">
                <h2 style="color: #fff">
                    {{ $page->meta_description }}
                </h2>
            </div>
        </div>
    </div>

    <div class="container mt-60">
        {!! $page->body !!}
    </div>

@stop

