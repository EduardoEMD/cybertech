@extends('layouts.index')

@section('menu')
    {{ menu('main', 'menus.main-menu-template') }}
@stop

@section('content')

<div id="home-area" class="height-100vh bg-oapcity-40 bg-overly bg-img-1 sm-height-none">
    <div class="table">
        <div class="table-cell">
            <div class="container">
                <div class="row">
                    <div class="home-slider clearfix">
                        <div class="col-md-8 col-sm-8">
                            <div class="top-text pt-60">
                                <div class="slider-text">
                                    <h1><span class="cyberRed">Cyber</span> Tech</h1>
                                    <h2>creating the future</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa. Pellentesque mollis eros vel mattis tempor. Aliquam eu efficitur enim, vitae fermentum orci. Sed et feugiat nulla. </p>
                                    <div class="button-set">
                                        <a class="button active" href="#">
                                            <i class="zmdi zmdi-facebook"></i>
                                            <span>
                                                <small>փնտրեք մեզ</small>
                                                <span class="large-text">Facebook</span>
                                            </span>
                                        </a>
                                        <a class="button btn-mobile" href="#">
                                            <i class="zmdi zmdi-instagram"></i>
                                            <span>
                                                <small>ինչպես նաև</small>
                                                <span class="large-text">Instagram</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="">
                                <img src="img/mobile/1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <canvas id="playArea"></canvas>
    </div> 
</div>
<div id="service-area">
    <div class="pt-100 pb-120">
        <div class="container">
            <div class="row text-center">
                @foreach($services as $service)
                <div class="col-sm-12 col-md-4" data-aos="zoom-in" data-aos-delay="420">
                        <div class="single-item single-service text-center">
                            <a href="{{ url('/pages/'.$service->slug) }}">
                                <div class="single-item-icon">
                                    {!! $service->icon !!}
                                </div>
                                <h4 class="ht-pt">{{ $service->title }}</h4>
                                <p>{{ $service->excerpt }}</p>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="atwork-banner"></div>
<div id="about-area" class="all-about gray-bg">
    <div class="container">
        <div class="row">
            <div class="home-slider2 clearfix">
                <div class="col-md-7 col-sm-6" data-aos="zoom-in" data-aos-delay="550">
                    <div class="about-bottom-left clearfix pt-120">
                        <h1> <span class="cyberRed">Cyber</span> <span class="cyberBlue">Tech</span> <small>- ի մասին</small></h1>
                        <h3>Lorem ipsum dolor sit amet </h3>
                        <p class="about-pb">Lorem Ipsum-ը տպագրության և տպագրական արդյունաբերության համար նախատեսված մոդելային տեքստ է: Սկսած 1500-ականներից` Lorem Ipsum-ը հանդիսացել է տպագրական արդյունաբերության ստանդարտ մոդելային տեքստ, ինչը մի անհայտ տպագրիչի կողմից տարբեր տառատեսակների օրինակների գիրք ստեղծելու ջանքերի արդյունք է: Այս տեքստը ոչ միայն կարողացել է գոյատևել հինգ դարաշրջան, այլև ներառվել է էլեկտրոնային տպագրության մեջ` մնալով էապես անփոփոխ: Այն հայտնի է դարձել 1960-ականներին Lorem Ipsum բովանդակող Letraset էջերի թողարկման արդյունքում, իսկ ավելի ուշ համակարգչային տպագրության այնպիսի ծրագրերի թողարկման հետևանքով, ինչպիսին է Aldus PageMaker-ը, որը ներառում է Lorem Ipsum-ի տարատեսակներ:</p>
                        <div class="about-icon">
                            <ul>
                                <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6">
                    <div class="about-bottom-img">
                        <img src="img/mobile/2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="icon-slider-area ">
    <div class="container-fluid">
        <div class="title-style title-pb text-center">
            <h1>Our Clients</h1>
            <div>
                <div class="clients-slider-single">
                    <div>
                        <p>
                            Lorem Ipsum-ը տպագրության և տպագրական արդյունաբերության համար նախատեսված մոդելային տեքստ է: Սկսած 1500-ականներից` Lorem Ipsum-ը հանդիսացել է տպագրական արդյունաբերության ստանդարտ մոդելային տեքստ, ինչը մի անհայտ տպագրիչի կողմից տարբեր տառատեսակների օրինակների գիրք ստեղծելու ջանքերի արդյունք է:
                        </p>
                        <div class="reating-icon">
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-half"></i>
                        </div>
                    </div>
                    <div>
                        <p>
                            Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text seit 1500, als ein unbekannter Schriftsteller eine Hand voll Wörter nahm und diese durcheinander warf um ein Musterbuch zu erstellen. Es hat nicht nur 5 
                        </p>
                        <div class="reating-icon">
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-half"></i>
                        </div>
                    </div>
                    <div>
                        <p>
                            Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации 
                        </p>
                        <div class="reating-icon">
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-half"></i>
                        </div>
                    </div>
                    <div>
                        <p>
                            ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლ
                        </p>
                        <div class="reating-icon">
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-half"></i>
                        </div>
                    </div>
                    <div>
                        <p>
                            Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal 
                        </p>
                        <div class="reating-icon">
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-half"></i>
                        </div>
                    </div>
                    <div>
                        <p>
                            Постојат многу варијации на пасуси од Lorem Ipsum, меѓутоа повеќето претрпеле одредени промени, некои со додавање на одредена доза на хумор, додека некои со случано подредување на зборовите
                            Постојат многу варијации на пасуси од Lorem Ipsum, меѓутоа повеќето претрпеле одредени промени, некои со додавање на одредена доза на хумор, додека некои со случано подредување на зборовите
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="clients-slider">
                    <div class="client-block">
                        <img src="img/company1.jpg">
                    </div>
                    <div class="client-block">
                        <img src="img/company2.jpg">
                    </div>
                    <div class="client-block">
                        <img src="img/company3.jpg">
                    </div>
                    <div class="client-block">
                        <img src="img/company4.jpg">
                    </div>
                    <div class="client-block">
                        <img src="img/company5.jpg">
                    </div>
                    <div class="client-block">
                        <img src="img/company6.jpg">
                    </div>		
                </div>
            </div>
        </div>
    </div>
</div>

    <div id="features-area" class=" ptb-120 fix">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="feature-title title-style title-pb">
                        <h1><span>Our Products </span></h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa. Pellentesque mollis eros vel mattis tempor. Aliquam </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fulwidth fix">
            <div class="col-lg-4 col-md-12">
                <div class="feature-list">
                @foreach($products as $product)
                    <div class="single-features-list normal-device {{ $loop->index == 2 ? 'mrn-nn' : ' '   }} {{ $loop->index >= 3 ? 'sfl-lsft' : ' ' }}">
                        <div class="feature-list-icon">
                            {!! $product->icon !!}
                        </div>
                        <div class="feature-list-text text-right">
                            <h3>{{ $product->title }}</h3>
                            <p>
                                <a href="{{ url('/pages/'.$product->slug) }}">
                                    {{ $product->excerpt }}
                                </a>
                            </p>
                        </div>
                    </div>
                    @if($loop->index == 2)
                    <div class="col-lg-4 col-md-12 text-center">      
                        <div class="feature-top">
                            <div class="feature-img">
                                <img src="img/mobile/3.png" alt="">
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <section class="latest-blog-area fix text-center container">
        <div class="latest-blog column fix section-margin">
            <div class="section-heading">
                <h2>LATEST NEWS FROM BLOG</h2>
                <p>Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Vivamus magna justo lacinia eget consectetur.</p>
                <div class="section-border"></div>
            </div>
            <div class="blog-content-area fix owl-carousel">
                @foreach($blog_news as $blog)
                    <div class="single-blog">
                        <div class="single-blog-img fix">
                            <a href=""><img src="{{ Voyager::image($blog->image) }}" alt=""></a>                
                            <div class="single-blog-hover">
                                <p>{{ $blog->title }}</p>
                            </div>
                        </div>
                        <div class="single-blog-title">
                            <a href="">
                                <h2>{{ $blog->category->name }}</h2>
                            </a>
                        </div>
                        <div class="single-blog-tag">
                            <p>
                                @foreach(explode(',',$blog->meta_keywords) as $keyword)
                                <a href="#">
                                    {{ '#'.trim($keyword) }}
                                    @if(!$loop->last) , @endif
                                </a>
                                   
                                @endforeach
                            </p>
                        </div>
                    </div> 
                @endforeach
            </div>
        </div>
    </section>

    <div id="vacancy-area">
        <div class="container">
            <div class="title-style text-center" style="margin-bottom: 55px">
                <h1>Թափուր աշխատատեղեր</h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, ullam? Labore dolorem vero eum ratione quis 
                </p>
            </div> 
            <div class="vacancy-items">
                <div class="row vacancy">
                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6">
                        <img src="img/node.png" class="vacancy-logo">
                    </div>
                    <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6">
                        <p class='vacancy-details'>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, debitis rem inventore, aut labore voluptatibus facilis vel deserunt et, numquam ducimus beatae dicta harum iste rerum odio aliquam voluptatem! Odio.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, debitis rem inventore, aut labore voluptatibus facilis vel deserunt et, numquam ducimus beatae dicta harum iste rerum odio aliquam voluptatem! Odio.
                        </p>
                        <div class="vacancy-open-modal" data-toggle='modal' data-target='#vacancy-modal' data-job="Node.js Developer">
                            <i class="zmdi zmdi-assignment">
                                <span>դիմել</span>
                            </i>
                        </div>
                    </div>
                </div>
                <div class="row vacancy">
                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6">
                        <img src="img/angular.png" class="vacancy-logo">
                    </div>
                    <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6">
                        <p class='vacancy-details'>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, debitis rem inventore, aut labore voluptatibus facilis vel deserunt et, numquam ducimus beatae dicta harum iste rerum odio aliquam voluptatem! Odio.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, debitis rem inventore, aut labore voluptatibus facilis vel deserunt et, numquam ducimus beatae dicta harum iste rerum odio aliquam voluptatem! Odio.
                        </p>
                        <div class="vacancy-open-modal" data-toggle='modal' data-target='#vacancy-modal' data-job="Angular.js Developer">
                            <i class="zmdi zmdi-assignment">
                                <span>դիմել</span>
                            </i>
                        </div>
                    </div>
                </div>
                <div class="row vacancy">
                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6">
                        <img src="img/seo.png" class="vacancy-logo">
                    </div>
                    <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6">
                        <p class='vacancy-details'>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, debitis rem inventore, aut labore voluptatibus facilis vel deserunt et, numquam ducimus beatae dicta harum iste rerum odio aliquam voluptatem! Odio.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, debitis rem inventore, aut labore voluptatibus facilis vel deserunt et, numquam ducimus beatae dicta harum iste rerum odio aliquam voluptatem! Odio.
                        </p>
                        <div class="vacancy-open-modal" data-toggle='modal' data-target='#vacancy-modal' data-job="SEO manager">
                            <i class="zmdi zmdi-assignment">
                                <span>դիմել</span>
                            </i>
                        </div>
                    </div>
                </div>
                <div class="row vacancy">
                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6">
                        <img src="img/csharp.png" class="vacancy-logo">
                    </div>
                    <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6">
                        <p class='vacancy-details'>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, debitis rem inventore, aut labore voluptatibus facilis vel deserunt et, numquam ducimus beatae dicta harum iste rerum odio aliquam voluptatem! Odio.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, debitis rem inventore, aut labore voluptatibus facilis vel deserunt et, numquam ducimus beatae dicta harum iste rerum odio aliquam voluptatem! Odio.
                        </p>
                        <div class="vacancy-open-modal" data-toggle='modal' data-target='#vacancy-modal' data-job="C# enginner">
                            <i class="zmdi zmdi-assignment">
                                <span>դիմել</span>
                            </i>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>		
    

    
    <div id="contact-area" class="contact-area gray-bg pt-120">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="title-style title-pb">
                        <h1>Կապ մեզ հետ</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa. Pellentesque mollis eros vel mattis tempor. Aliquam </p>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div id="map"></div>
                </div>
                <div class="col-md-6 col-xs-12 text-center">
                    <div class="contact-from">
                        <form id="message-form" method="post">
                            <input id="fullname" type="text" placeholder="Անուն ազգանուն">
                            <input id="phone" type="text" placeholder="հեռախոսահամար">
                            <input id="email" type="email" placeholder="էլ․ հասցե">
                            <textarea id="message" placeholder="հաղորդագրություն"></textarea>
                            <input class="submit" type="submit" value="ուղարկել">
                        </form>
                        <p class="form-messege"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="vacancy-modal" class="modal fade " role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title job-vacancy-title">- - -</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row vacancy-form-container">
                            <div class="col-md-6 col-sm-12 col-xs-12 column1">
                                <div class="col-md-12">
                                    <img src="img/node.png" class="vacancy-modal-logo">
                                </div>
                                <div class="col-md-12">
                                    <p style="text-align: justify;" class="vacancy-modal-details">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea voluptatum provident repellat quasi perferendis, inventore numquam vitae, temporibus harum illum totam reprehenderit suscipit aliquid nesciunt quis amet accusantium magni, dolor?
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea voluptatum provident repellat quasi perferendis, inventore numquam vitae, temporibus harum illum totam reprehenderit suscipit aliquid nesciunt quis amet accusantium magni, dolor?
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea voluptatum provident repellat quasi perferendis, inventore numquam vitae, temporibus harum illum totam reprehenderit suscipit aliquid nesciunt quis amet accusantium magni, dolor?
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 column2">
                                <div class="contact-from">
                                    <form method="post" id="cv-form">
                                        <section class="heading">
                                            <input name="fullname" type="text" placeholder="Անուն ազգանուն">
                                            <input name="phone" type="text" placeholder="հեռախոսահամար">
                                            <input name="email" type="email" placeholder="էլ․ հասցե">
                                            <textarea name="message" placeholder="հաղորդագրություն"></textarea>

                                            <div class="file-uploader">
                                                <input name="cv" type="file">
                                                <div class="attacher">
                                                    <i class="zmdi zmdi-attachment-alt"></i>
                                                    <span>վերբեռնել CV</span>
                                                </div>
                                            </div>
                                        </section>
                                        
                                        <section class="footer">
                                            <input class="submit" type="submit"  value="ուղարկել հայտ">
                                        </section>
                                    </form>
                                    <p class="form-messege"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">չեղարկել</button>
                </div>
            </div>

        </div>
    </div>

@stop
