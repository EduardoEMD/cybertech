@extends('layouts.blog-template')


@section('content')

<div class="container">
<div class='theme-opt' style='display:none'>
    <div class='ibpanel section' id='ibpanel' name='Theme Options'>
        <div class='widget HTML' data-version='1' id='HTML905'>
            The Magazine
        </div>
        <div class='widget HTML' data-version='1' id='HTML904'>
            
        </div>
    </div>
</div>



<div id="outer-wrapper" class="index home">
    <div class='clear'></div>
    <div class='row' id='content-wrapper'>
        <div class='clear'></div>
        <div id='main-wrapper'>
            <div class='slider-wrapper' id='slider-wrapper'>
                <div class="slider-content">
                    <div class="slider-sec section" id="slider-sec" name="Main Slider">
                        <div class="widget HTML" data-version="1" id="HTML3">
                            @include("partial.blog-main-slider")
                        </div>  
                    </div>  
                </div>  
            </div>
            <div class='main section' id='main' name='Main Posts'>
                <div class='widget Blog' data-version='1' id='Blog1'>
                    <div class='blog-posts hfeed'>
                        <!--Can't find substitution for tag [defaultAdStart]-->
                        <div class='post-outer'>
                            @forelse($posts as $post)
                            <div class='post'>
                                <div class='block-image'>
                                    <div class='thumb'>
                                        <a href='{{ url('/blog/' . $post->slug) }}' style='background:url({{ Voyager::image($post->image) }}) no-repeat center center;background-size:cover'>
                                            <span class='thumb-overlay' />
                                        </a>
                                    </div>
                                </div>
                                <div class='post-header'>
                                </div>
                                <article>
                                    <font class='retitle'>
                                        <h2 class='post-title entry-title'>
                                            <a href='{{ url('/blog/' . $post->slug) }}'>
                                                {{ truncate($post->title, 100) }}
                                            </a>
                                        </h2>
                                    </font>
                                    <div class='date-header'>
                                        <div id='meta-post'>
                                            <a class='timestamp-link' href='{{ url('/blog/' . $post->slug) }}'
                                                rel='bookmark' title='permanent link'>
                                            <abbr class='published' itemprop='datePublished dateModified' title='August 01, 2016'>{{ $post->created_at->format("M d, Y") }}</abbr>
                                            </a>
                                            <div class='resumo'>
                                                <span> {{ $post->excerpt }}...</span>
                                            </div>
                                        </div>
                                        <div style='clear: both;'></div>
                                    </div>
                                </article>
                                <div style='clear:both'></div>
                                <div class='post-footer'>
                                </div>
                            </div>
                            @empty
                                <h4>Տվյալները բացակայում են ․ ․ ․</h4>           
                            @endforelse
                            
                                </div>
                                <!--Can't find substitution for tag [adEnd]-->
                                <div class='inline-ad'>
                                </div>
                                <!--Can't find substitution for tag [adStart]-->
                                <!--Can't find substitution for tag [adEnd]-->
                            </div>
                            
                            <div class='blog-pager' id='blog-pager'>
                                {{ $posts->links() }}
                            </div>
                            <div class='clear'></div>
                            <div class='blog-feeds'>
                                <div class='feed-links'>
                                    Subscribe to:
                                    <a class='feed-link' href='http://writeup-themexpose.blogspot.com/feeds/posts/default' target='_blank' type='application/atom+xml'>
                                        Posts ( Atom )
                                    </a>
                                </div>
                            </div>
                            <script type="text/javascript">window.___gcfg = { 'lang': 'en' };</script>
                        </div>
                        <div class='widget HTML' data-version='1' id='HTML300'>
                        </div>
                        <div class='widget HTML' data-version='1' id='HTML301'>
                        </div>
                    </div>
                </div>
                
                @include("partial.blog-sidebar")

                <div class='clear'></div>
            </div>
            </div>
@stop