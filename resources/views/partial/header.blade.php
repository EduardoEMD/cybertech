<header class="header-area">
	<div id="main-menu" class="sticker stick slideDown">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 main-navigation-container">
					<div class="logo float-left">
						<a href="index.html">
							<img src="{{ url('storage/' . setting('site.logo')) }}" alt="logo">
						</a>
					</div>
					<div class="main-menu float-right">
							@yield('menu')
					</div>                            
				</div>                            
				<div class="col-md-12 col-sm-12 hidden-lg hidden-md">                                
					<div class="mobile-menu">                                   
						<nav style="display: block;">                          
							@yield('menu')
						</nav>                                
					</div> 
				</div>                        
			</div>                    
		</div>                
	</div>            
</header>