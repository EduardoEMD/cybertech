
<div id="footer-wrapper">
        <div class="row" id="footer">
            <div class="sect-left ready-widget section" id="footer-sec1" name="Widget 1">
                <div class="widget HTML" data-version="1" id="HTML17">
                    <h2 class="title">Recent Posts</h2>
                    <div class="widget-content">
                        <div class="recentposts">
                            <ul class="custom-widget">
                                    @include("partial.post-list", ["posts" => $recent_posts])
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sect-left ready-widget section" id="footer-sec2" name="Widget 2">
                <div class="widget HTML" data-version="1" id="HTML16">
                    <h2 class="title">Random Posts</h2>
                    <div class="widget-content">
                        <div class="randomposts">
                            <ul class="custom-widget">
                                    @include("partial.post-list", ["posts" => $random_posts])
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sect-left ready-widget section" id="footer-sec2" name="Widget 2">
                <div class="widget HTML" data-version="1" id="HTML16">
                    <h2 class="title">Popular Posts</h2>
                    <div class="widget-content">
                        <div class="randomposts">
                            <ul class="custom-widget">
                                    @include("partial.post-list", ["posts" => $popular_posts])
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="clear"></div>
        <div class="footer-wrapper">
            <div class="footer-copy row">
                <div class="copyright"> Created By
                    <a href="http://themexpose.com" id="mycontent" rel="dofollow" title="Free Blogger Templates">ThemeXpose</a> &amp;
                    <a href="http://www.freedesignresource.com/" id="mycontent1" rel="dofollow" title="Blogger Template">Free Design Resource</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id='back-to-top'></div>
<!-- blocked widgets -->
<div style='display: none'>
    <div class='blocked-widgets section' id='blocked-widgets'>
        <div class='widget Attribution' data-version='1' id='Attribution1'>
            <div class='widget-content' style='text-align: center;'>
                Powered by
                <a href='https://www.blogger.com' target='_blank'>Blogger</a>.
            </div>
            <div class='clear'></div>
            <span class='widget-item-control'>
                <span class='item-control blog-admin'>
                    <a class='quickedit' href='//www.blogger.com/rearrange?blogID=4819543551673866339&widgetType=Attribution&widgetId=Attribution1&action=editWidget&sectionId=blocked-widgets'
                        onclick='return _WidgetManager._PopupConfig(document.getElementById("Attribution1"));' target='configAttribution1'
                        title='Edit'>
                        <img alt='' height='18' src='https://resources.blogblog.com/img/icon18_wrench_allbkg.png' width='18' />
                    </a>
                </span>
            </span>
            <div class='clear'></div>
        </div>
        <div class='widget Navbar' data-version='1' id='Navbar1'>
          
            <div id="navbar-iframe-container"></div>
          
        </div>
    </div>
</div>

