@if(isset($slider_posts) && !empty($slider_posts))
<div id="featured-slider">
    <ul class="slides">
        @foreach($slider_posts as $post)
            <li class="flex-active-slide">
                <a class="slider-img" href="{{ url('/blog/' . $post->slug) }}">
                    <div class="feets" style="background-image:url({{ Voyager::image($post->image) }});"></div>
                </a>
                <div class="slide-bottom-caption">
                    <h1 class=""><a href="{{ url('/blog/' . $post->slug) }}">{{ $post->title }}</a></h1><span class="feat-divider"></span><span class="post-date">{{ $post->created_at->format("M d, Y") }}</span></div>
                <div class="slide-cap-bg"></div>
                <a href="{{ url('/blog/' . $post->slug) }}" class="slide-overlay"></a>
            </li>
        @endforeach
    </ul>
</div>
@endif