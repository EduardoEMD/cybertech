@foreach($posts as $post)
<li>
    <a class="rcthumb img-effect" href="{{ url('/blog/' . $post->slug) }}" style="background:url({{ Voyager::image($post->image) }}) no-repeat center center;background-size: cover">
        <span class="img-overlay"></span>
    </a>
    <div class="post-panel">
        <h3 class="rcp-title">
            <a href='{{ url('/blog/' . $post->slug) }}'>
                {{ $post->excerpt }} 
            <span class="cyberRed">
                ավելին <i class="fa fa-arrow-circle-right"></i>
            </span> 
            </a>
        </h3>
    </div>
</li>
@endforeach