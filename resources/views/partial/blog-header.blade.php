
<div id='main-header'>
        <div class='tm-head group row'>
            <div id='logo-wrap'>
                <div class='section' id='header' name='Logo'>
                    <div class='widget Header' data-version='1' id='Header1'>
                        <div id='header-inner'>
                            <a href='{{ url('/') }}' style='display:block'>
                                <h1 style='display:none'></h1>
                                <img alt='Cybertech logo' height='120px; ' id='Header1_headerimg' src='{{ url("storage/".setting("site.logo")) }}'
                                    style='display:block' width='349px; ' />
                            </a>
                        </div>
                    </div>
                </div>
                <span id='tagline'></span>
            </div>
            <div id='header-elements'>
                <div id='header-social'>
                    <div class='social-top section' id='social-top' name='Social Top'>
                        <div class='widget LinkList' data-version='1' id='LinkList47'>
                            <div class='widget-content'>
                                <ul>
                                    <li>
                                        <a class='facebook' href='http://facebook.com' title='facebook'></a>
                                    </li>
                                    <li>
                                        <a class='twitter' href='http://twitter.com' title='twitter'></a>
                                    </li>
                                    <li>
                                        <a class='gplus' href='http://plus.google.com' title='gplus'></a>
                                    </li>
                                    <li>
                                        <a class='instagram' href='http://Instagram.com' title='instagram'></a>
                                    </li>
                                    <li>
                                        <a class='rss' href='http://feed.rss.com' title='rss'></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<nav id='main-nav'>
    <div class='header-menu row'>
        <ul class="main-menu">
            {{ menu("blog-menu", 'menus.blog-menu-template') }}
        </ul>

        <div class="mobile-menu">
            <div>
                <i class="fa fa-bars burger-icon" aria-hidden="true"></i>
            </div>
            <ul>
                {{ menu("blog-menu", 'menus.blog-menu-template') }}
            </ul>
        </div>
    </div>
</nav>