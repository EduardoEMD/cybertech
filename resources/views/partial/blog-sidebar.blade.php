<div id='sidebar-wrapper'>
    <div class='sidebar ready-widget section' id='sidebar2' name='Sidebar Right'>
        <div class='widget HTML' data-version='1' id='HTML1'>
            <h2 class='title'>Random Posts</h2>
            <div class='widget-content'>
                <div class="randomposts">
                    <ul class="custom-widget">
                        @include("partial.post-list", ['posts' => $random_posts])
                    </ul>
                <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class='widget HTML' data-version='1' id='HTML2'>
            <h2 class='title'>Follow Us</h2>
            <div class='widget-content'>
                <div class="social-count-plus">
                    <ul class="default">
                        <li class="count-facebook">
                            <a class="icon" href="https://www.facebook.com/WordPress"></a>
                            <span class="items" style="color: #fff">
                                <span class="count" style="color: #fff">1113957</span>
                                <span class="label" style="color: #fff">likes</span>
                            </span>
                        </li>
                        <li class="count-twitter">
                            <a class="icon" href="https://twitter.com/AlxMedia"></a>
                            <span class="items" style="color: #fff">
                                <span class="count" style="color: #fff">441</span>
                                <span class="label" style="color: #fff">followers</span>
                            </span>
                        </li>
                        <li class="count-youtube">
                            <a class="icon" href="https://www.youtube.com/user/Envato"></a>
                            <span class="items" style="color: #fff">
                                <span class="count" style="color: #fff">758</span>
                                <span class="label" style="color: #fff">subscribers</span>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class='clear'></div>
            <span class='widget-item-control'>
                <span class='item-control blog-admin'>
                    <a class='quickedit' href='//www.blogger.com/rearrange?blogID=4819543551673866339&widgetType=HTML&widgetId=HTML2&action=editWidget&sectionId=sidebar2'
                        onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML2"));' target='configHTML2'
                        title='Edit'>
                        <img alt='' height='18' src='https://resources.blogblog.com/img/icon18_wrench_allbkg.png' width='18' />
                    </a>
                </span>
            </span>
            <div class='clear'></div>
        </div>
        <div class='widget HTML' data-version='1' id='HTML23'>
            <h2 class='title'>Facebook</h2>
            <div class='widget-content'>
                <center>
                        <div class="fb-page" 
                        data-href="https://www.facebook.com/CyberTechArmenia/"
                        data-width="360" data-small-header="false"
                        data-hide-cover="false"
                        data-show-facepile="false">
                    </div>
                </center>
            </div>
        </div>
        <!-- <div class='widget FollowByEmail' data-version='1' id='FollowByEmail1'>
            <h2 class='title'>Follow by Email</h2>
            <div class='widget-content'>
                <div class='follow-by-email-inner'>
                    <form action='https://feedburner.google.com/fb/a/mailverify' method='post' onsubmit='window.open("https://feedburner.google.com/fb/a/mailverify?uri=blogspot/qftaF", "popupwindow", "scrollbars=yes,width=550,height=520"); return true'
                        target='popupwindow'>
                        <table width='100%'>
                            <tr>
                                <td>
                                    <input class='follow-by-email-address' name='email' placeholder='Email address...' type='text' />
                                </td>
                                <td width='64px'>
                                    <input class='follow-by-email-submit' type='submit' value='Submit' />
                                </td>
                            </tr>
                        </table>
                        <input name='uri' type='hidden' value='blogspot/qftaF' />
                        <input name='loc' type='hidden' value='en_US' />
                    </form>
                </div>
            </div>
            <span class='item-control blog-admin'>
            </span>
        </div> -->
        <div class='widget HTML' data-version='2' id='HTML2'>
            <h2 class='title'>Popular Posts</h2>
            <div class='widget-content'>
                <div class="randomposts">
                    <ul class="custom-widget">
                        @include("partial.post-list", ['posts' => $popular_posts])
                    </ul>
                <div class="clear"></div>
                </div>
            </div>
        </div>
        
        <div class='widget Label' data-version='1' id='Label2'>
            <h2>Tags</h2>
            <div class='widget-content cloud-label-widget-content'>
                @foreach($categories as $categorie)
                    <span class='label-size label-size-1'>
                        <a dir='ltr' href='{{ url('/blog?search=' . $categorie->slug) }}'>{{ $categorie->name }}</a>
                    </span>
                @endforeach
            </div>
        </div>
    </div>
</div>