@extends('layouts.blog-template')


@section('content')

    <div id="outer-wrapper" class="index home">
    <div class='clear'></div>
    <div class='row' id='content-wrapper'>
        <div class='clear'></div>
        <div id='main-wrapper'>
            <div class='main section' id='main' name='Main Posts'>
                <div class='widget Blog' data-version='1' id='Blog1'>
                    <div class='blog-posts hfeed'>
                        <div class="post">
                            <div class='post-outer'>
                                    <div class="post-header">
                                        <div class="post-head">
                                            <h1 class="post-title entry-title" itemprop="name headline">
                                                {{ $post->title }}
                                            </h1>
                                        </div>
                                        <div class="post-meta">
                                            <span class="post-author vcard"></span>
                                            <span class="post-timestamp">
                                                <abbr class="published" itemprop="datePublished dateModified" title="{{ $post->created_at->format('M d, Y') }}">{{ $post->created_at->format('M d, Y') }}</abbr>
                                            </span>
                                        </div>
                                        <br>
                                        <div class="separator" style="clear: both; text-align: center;">
                                            <img border="0" data-original-height="847" data-original-width="1270" src="{{ Voyager::image($post->image) }}">
                                            <div class="share-buttons">
                                                <div class="share-button fb-share-button"
                                                        data-href="{{ Request::url() }}"
                                                        data-layout="button" data-size="large" data-mobile-iframe="true">
                                                </div>
                                                <div class="share-button">
                                                    <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-size="large" data-show-count="false">Tweet</a>
                                                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <article>
                                        <div class="post-body post-body entry-content">

                                            <div>
                                                {!! $post->body !!}
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                </div>
                                <div class='clear'></div>
                                <div class='blog-feeds'>
                                    <div class='feed-links'>
                                        Subscribe to:
                                        <a class='feed-link' href='http://writeup-themexpose.blogspot.com/feeds/posts/default' target='_blank' type='application/atom+xml'>
                                            Posts ( Atom )
                                        </a>
                                    </div>
                                </div>
                                <script type="text/javascript">window.___gcfg = { 'lang': 'en' };</script>
                            </div>

                        </div>
                        <div class='widget HTML' data-version='1' id='HTML300'>
                        </div>
                        <div class='widget HTML' data-version='1' id='HTML301'>
                        </div>
                    </div>
                </div>
                
                @include("partial.blog-sidebar")
                <div class='clear'></div>
            </div>
            </div>

@stop