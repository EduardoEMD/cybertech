<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ setting('site.title') }}</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('css/venobox.css') }}">
	<link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/slick.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}">
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style-customizer.css') }}">
	<link rel="stylesheet" href="{{ asset('src/aos/aos.css') }}">
	<link rel="stylesheet" href="{{ asset('css/chaos.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('src/lobibox/Lobibox.min.css') }}"/>

    <script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
    <script src="{{ asset('src/lobibox/Lobibox.min.js') }}"></script>
</head>
<body>
    <input type="hidden" id="base" value="{{ url('') }}">
    {{ csrf_field() }}
    @include("partial.header")
    
    @yield('content')

    <footer>
        <div>
            <div class="conatct-info fix">
                <div class="col-md-4 col-sm-4 text-center">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="zmdi zmdi-phone"></i>
                        </div>
                        <div class="contact-text">
                            <span>
                                +012 345 678 102
                                <br>
                                +012 345 678 102
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 text-center">
                    <div class="single-contact-info res-xs2">
                        <div class="contact-icon">
                            <i class="zmdi zmdi-globe-alt"></i>
                        </div>
                        <div class="contact-text">
                            <span>
                                <a href="#">sybertech@gmail.com </a>
                                <br>
                                <a href="#">esiminch.com</a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 text-center">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="zmdi zmdi-pin"></i>
                        </div>
                        <div class="contact-text">
                            <span>
                                Արշակունյաց 2,
                                <br>
                                Մամուլի շ․ 5-րդ հարկ.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
<script src="{{ asset('js/owl.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('src/aos/aos.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modernizr-2.8.3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/googleMap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/canvas-animation.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcSMa9LSWLopBGiowyGRQV1oVOJja3YPE&callback=initMap">
</script>
</html>