<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='//fonts.googleapis.com/css?family=Oswald:300,400,700|Open+Sans:400,400italic,700,700italic' media='all' rel='stylesheet' type='text/css' />
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300italic,300,400italic,700,latin-ext' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="{{ asset('css/blog.css') }}">    
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('src/owlcarousel/owl.carousel.min.css') }}"> -->
    <!-- <link rel="stylesheet" href="{{ asset('src/owlcarousel/owl.theme.default.min.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('src/slick/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('src/slick/slick-theme.css') }}"/>
    


    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' type='text/javascript'></script>
    <script src="{{ asset('src/owlcarousel/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('src/slick/slick.min.js') }}"></script>
    <script src="{{ asset('js/blog.js') }}"></script>
    <title>Document</title>
</head>
<body class="index">

    @include("partial.blog-header")

    @yield('content')

    @include("partial.blog-footer")

</body>
</html>