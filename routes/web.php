<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController');
Route::get('/pages/{slug}', 'PageController@show');

Route::post('/new-message', 'HomeController@send_message');
Route::post('/new-applicant', 'HomeController@new_applicant');


Route::group(['prefix' => 'blog'], function() 
{
    Route::get('/', "BlogController");
    Route::get('{slug}', "BlogController@show");
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
