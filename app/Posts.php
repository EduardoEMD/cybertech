<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
  
    public static function findBySlug($slug)
    {
        return static::where('slug', $slug)->firstOrFail();
    }

    public static function get_blog_slider_posts($count){
        return static::where("data_analysis", "blogslider")->orderBy("id", "desc")->take($count)->get();
    }

    public static function get_popular_posts($count){
        return static::where("data_analysis", "popular")->orderBy("id", "desc")->inRandomOrder()->take($count)->get();
    }

    public function category(){
        return $this->hasOne("App\Categories", "id",'category_id');
    }
}
