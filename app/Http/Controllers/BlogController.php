<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Posts;
use App\Categories;

class BlogController extends Controller
{
    
    public function __invoke(Request $request)
    {
        $keyword = $request->input("search");

        if(!empty($keyword)){
            $categorie = Categories::where("slug", $keyword)->firstOrFail();

            $posts = Posts::where("category_id",$categorie->id )->orderBy("id", 'desc')->paginate(5);
        }else{
            $posts = Posts::orderBy("id", 'desc')->paginate(5);
        }

        $slider_posts  = Posts::get_blog_slider_Posts(4);
        $random_posts  = Posts::inRandomOrder()->take(3)->get();
        $popular_posts = Posts::get_popular_posts(3);
        $recent_posts  = Posts::orderBy("id", "desc")->take(3)->get(); 
        $categories    = Categories::all();
        
        $data = ["posts", "slider_posts", 'random_posts', 'popular_posts', "recent_posts",'categories'];

        return view("blog", compact($data));
    }

    public function show($slug)
    {
        $post = Posts::findBySlug($slug);
        
        $slider_posts  = Posts::get_blog_slider_Posts(4);
        $random_posts  = Posts::inRandomOrder()->take(3)->get();
        $popular_posts = Posts::get_popular_posts(3);
        $recent_posts  = Posts::orderBy("id", "desc")->take(3)->get(); 
        $categories    = Categories::all();


        $data = ["post", "slider_posts", 'random_posts', 'popular_posts', "recent_posts",'categories'];

        return view('post-content',compact($data));
    }
}
