<?php

namespace App\Http\Controllers;

use App\Pages;

class PageController extends Controller
{
    public function show($slug)
    {
        $page = Pages::findBySlug($slug);

        $data = [$page];
        return view('page', compact('page'));
    }
}
