<?php

namespace App\Http\Controllers;

use App\libraries\Mailer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Pages;
use App\Posts;

class HomeController extends Controller
{
    public function __invoke()
    {
        $services = Pages::where('type', 'service')->take(3)->get();
        $products = Pages::where("type", 'product')->take(8)->get();

        $part1 = Posts::orderBy("id", "desc")->where("category_id", "1")->take(2);
        $part2 = Posts::orderBy("id", "desc")->where("data_analysis", "homepage")->take(2);
        
        $blog_news = $part1->union($part2)->get();

        $data = ['services', 'blog_news', 'products'];

        return view("home", compact($data));
    }

    public function send_message(Request $request)
    {
        $email = $request->input("email");
        $fullname = $request->input("fullname");
        $phone = $request->input("phone");
        $message = $request->input("message");
        $date = Carbon::now();

        $content = "
            <h1>Նոր նամակ cybertech -ից</h1>

            <h3>
                <b>Անուն - ազգանուն ։ </b> $fullname <br>
                <b>հեռախոսահամար    ։ </b> $phone <br>
                <b>էլ․ հասցե        ։ </b> $email <br>
                <b>հաղորդագրություն ։ </b>
                <p>
                    <i><u> \t $message </u></i>
                </p>
                <hr>
                <b> Ուղարկվել է՝ </p> $date
            </h3>
        ";
        if (Mailer::send($email, $fullname, "edhovhannisyan97@gmail.com", "CyberTech user-message", $content)) {
            echo "sent";
        } else {
            echo "error";
        }
    }

    public function new_applicant(Request $request)
    {
        $email = $request->input("email");
        $fullname = $request->input("fullname");
        $phone = $request->input("phone");
        $message = $request->input("message");
        $jobTitle = $request->input("jobTitle");

        $file = $request->file('cv');
        $date = Carbon::now();
        $attachment = null;

        if(!empty($file)){

            $attachment = [
                'path' => $file->getPathName(),
                'name' => $file->getClientOriginalName(),
            ];
        }
        

        $content = "
            <h1>Նոր Դիմորդ cybertech -ից</h1>

            <h3>
                <b>Անուն - ազգանուն ։ </b> $fullname <br>
                <b>հեռախոսահամար    ։ </b> $phone <br>
                <b>էլ․ հասցե        ։ </b> $email <br>
                <b>դիմած աշխատանք   ։ </b> $jobTitle <br>
                <b>հաղորդագրություն ։ </b>
                <p>
                    <i><u> \t $message </u></i>
                </p>
                <hr>
                <b> Ուղարկվել է՝ </p> $date
            </h3>
        ";

        if (Mailer::send($email, $fullname, "edhovhannisyan97@gmail.com", "CyberTech user-message", $content, $attachment)) {
            echo "sent";
        } else {
            echo "error";
        }
    }
}
