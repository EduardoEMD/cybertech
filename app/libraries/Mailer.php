<?php

namespace App\libraries;

use PHPMailer\PHPMailer\PHPMailer;

class Mailer
{
    public static function send($from, $fullname, $to, $subject, $content, $file = null)
    {
        //Load Composer's autoloader
        require base_path() . '/vendor/PHPMailer/PHPMailer/src/Exception.php';
        require base_path() . '/vendor/PHPMailer/PHPMailer/src/PHPMailer.php';
        require base_path() . '/vendor/PHPMailer/PHPMailer/src/SMTP.php';

        $mail = new PHPMailer(true);

        // Passing `true` enables exceptions;
        //Server settings
        $mail->SMTPDebug = 0; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = gethostbyname('smtp.gmail.com'); // Specify main and backup SMTP servers
        $mail->CharSet = 'UTF-8';
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = 'ed.hovhannisyan99@gmail.com'; // SMTP username
        $mail->Password = 'eh160899google'; // SMTP password
        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587; // TCP port to connect to

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true,
            ),
        );

        //Recipients
        $mail->setFrom($from, $fullname);
        $mail->addAddress($to); // Add a recipient

        if (!empty($file)) {
            $mail->AddAttachment($file['path'], $file['name']);

        }

        //Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $content;
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        return $mail->send();

    }
}
